new Vue({
    el: "#app",
    data: {
        editStatus: false,
        form: {
            postTitle: "",
            postStory: "",
            postAuthor: ""
        },
        entries: []
    },
    methods: {
        submitForm: function(event) {
            if (this.editStatus == false) {
                var newPost = Object.assign({}, this.form);
                newPost.timeStamp = moment(new Date()).format('Do MMMM YYYY (dddd) - hh:mma');
                this.entries.push(newPost);
            }
            this.set_data();
        },
        set_data: function() {
            localStorage.setItem('blogEntries', JSON.stringify(this.entries));
        },
        get_data: function() {
            return localStorage.getItem('blogEntries');
        },
        edit_post: function(post) {
            this.editStatus = true;
            this.form = post;
            this.scrollTop();
        },
        delete_post: function(key) {
            this.entries.splice(key, 1);
            this.set_data();
        },
        changeStatus: function() {
            if (this.editStatus) {
                this.editStatus = false;
                this.form = {};
            } else {
                this.editStatus = true;
    
            }
        },
        scrollTop: function() {
            window.scrollTo(0,0);
        }
    },
    mounted: function() {
        this.entries = JSON.parse(this.get_data()) || [];
    }
})